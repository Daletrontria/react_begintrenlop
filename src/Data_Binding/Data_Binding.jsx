import React, { Component } from "react";

export default class Data_Binding extends Component {
  userAge = 12;
  renderFooter = () => {
    return <h1>Most wanted Fucker</h1>;
  };
  render() {
    let username = "alice";
    let imgSrc =
      "https://play-lh.googleusercontent.com/kyp2sHm5j8Wg3ZZoKObeMDylyTzIYJc_WAZJId-EZptCH6nHR-_dHhVrWtUv5wgajI59yLfFJpbYe7u4SVeX";
    return (
      <div>
        <div>Hello {username}</div>
        <p>UserAge:{this.userAge}</p>
        <img
          style={{
            width: 300,
            marginTop: 100,
          }}
          src={imgSrc}
          alt=""
        />
        {this.renderFooter()}
      </div>
    );
  }
}
