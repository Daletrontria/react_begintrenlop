import React, { Component } from "react";

export default class DemoState extends Component {
  // Nơi lưu trữ dữ liệu liên quan để Component ~ thay đổi dữ liệu này thì cần giao diện mới tương ứng
  state = {
    like: 0,
    userInfor: {
      name: "Maria Ozawa",
    },
  };
  handlePlusLike = () => {
    //   set state dùng để update giá trị state
    // update not replace
    this.setState({
      like: this.state.like + 1,
    });
  };

  handleChangeName = () => {
    this.setState({
      userInfor: {
        name: "Shizuka",
      },
    });
  };
  //   State thay đổi thì giao diện chạy lại
  render() {
    console.log("Như Like Thần Chưởng");
    return (
      <div>
        <p>{this.state.like}</p>
        <button onClick={this.handlePlusLike} className="btn btn-secondary">
          Plus Like
        </button>
        <br />

        <p>{this.state.userInfor.name}</p>
        <button onClick={this.handleChangeName} className="btn btn-success">
          Change Maria Ozawa to Shizuka
        </button>
      </div>
    );
  }
}
