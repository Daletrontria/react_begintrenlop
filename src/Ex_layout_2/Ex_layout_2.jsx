import React, { Component } from "react";
import Banner from "./Banner";
import Header from "./Header";
import Item from "./Item";
import List from "./List";

export default class Ex_layout_2 extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <Item />
        <List />
      </div>
    );
  }
}
