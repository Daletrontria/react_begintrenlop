import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  IsLogin = true;
  handleLogin = () => {
    this.IsLogin = true;
    console.log("this.IsLogin", this.IsLogin);
  };

  renderContent = () => {
    if (this.IsLogin) {
      return (
        <>
          <p>Đã Đăng Nhập</p>
          <button className="btn btn-danger">Đăng Xuất</button>
        </>
      );
    } else {
      return (
        <>
          <p>Chưa Đăng Nhập</p>
          <button className="btn btn-success">Đăng Nhập</button>
        </>
      );
    }
  };

  render() {
    return (
      <div>
        {this.renderContent()}
        {/* support toán tử 3 ngôi */}
        {/* {this.IsLogin ? "Đã Đăng Nhập" : "Chưa Đăng Nhập"} */}
      </div>
    );
  }
}
