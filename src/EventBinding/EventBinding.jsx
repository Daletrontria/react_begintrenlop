import React, { Component } from "react";

export default class extends Component {
  handleSayHello = () => {
    console.log("Hello");
  };
  handleSayHelloByName = (name) => {
    console.log("Hello " + name);
  };

  render() {
    return (
      <div>
        <button onClick={this.handleSayHello} className="btn btn-success">
          Say hello
        </button>
        <br />
        <button
          onClick={() => {
            this.handleSayHelloByName("Maria Ozawa");
          }}
          className="btn btn-primary"
        >
          Say hello to Alice
        </button>
      </div>
    );
  }
}
