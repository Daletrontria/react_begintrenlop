import React, { Component } from "react";
import { renderIntoDocument } from "react-dom/test-utils";
import { data } from "./dataGlasses";
import "./renderWithMap.module.css";
export default class RenderWithMap extends Component {
  state = {
    glasses: data,
    glassDetail: data[0],
  };
  //   Hàm thay kính
  handleChangeGlass = (data) => {
    console.log("data", data);
    this.setState({
      glassDetail: data,
    });
  };
  renderList = () => {
    return this.state.glasses.map((item, index) => {
      console.log("item", item);
      return (
        <img
          key={index}
          onClick={() => {
            this.handleChangeGlass(item);
          }}
          // width:`${100/this.state.glasses.lenght}%`
          style={{ width: 200, padding: "0px 5px" }}
          src={item.url}
          alt=""
        ></img>
      );
    });
  };
  renderGlass = () => {
    return (
      <div className="tryOn">
        <div className="KM">
          <img
            style={{
              width: 200,
              padding: "0px 5px",
              top: "100px",
              className: "KM",
            }}
            src={this.state.glassDetail.url}
            alt=""
          ></img>
        </div>

        <div className="Model">
          <img
            src="./glassesImage/model.jpg"
            style={{
              width: "400px",
              padding: "0px 5px",
              height: "400px",
              className: "model",
            }}
            alt=""
          ></img>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div>
        {this.renderGlass()}
        {this.renderList()}
      </div>
    );
  }
}
