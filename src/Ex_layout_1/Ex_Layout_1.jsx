import React from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Home from "./Home";
import Navigate from "./Navigate";



export default function Ex_Layout_1() {
  return (
    <div>
      <Home />
      <Header />
      <div className="row">
        <div className="col-6">
          <Navigate />
        </div>
        <div className="col-6">
          <Content />
        </div>
      </div>
      <Footer />
    </div>
  );
}
