import logo from "./logo.svg";
import "./App.css";
import DemoFunction from "./DemoComponentNew/DemoFunction";
import DemoClass from "./DemoComponentNew/DemoClass";
import Ex_Layout_1 from "./Ex_layout_1/Ex_Layout_1";
import Ex_layout_2 from "./Ex_layout_2/Ex_layout_2";
import Data_Binding from "./Data_Binding/Data_Binding";
import EventBinding from "./EventBinding/EventBinding";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import DemoState from "./DemoState/DemoState";
import RenderWithMap from "./RenderWithMap/RenderWithMap";

function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* demo class */}

      {/* <DemoFunction /> */}

      {/* <Ex_layout_1 /> */}
      {/* <Ex_layout_2 /> */}
      {/* <Data_Binding /> */}
      {/* <EventBinding /> */}
      {/* <ConditionalRendering /> */}
      {/* <DemoState /> */}
      <RenderWithMap />
    </div>
  );
}

export default App;
